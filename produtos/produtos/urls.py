from django.contrib import admin
from django.urls import path, include
from produtosApp import urls as produtos_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(produtos_urls)),
]