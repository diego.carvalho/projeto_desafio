from django.db import models

class Produtos (models.Model):
    nome = models.CharField(max_length=30)
    cod_prod = models.IntegerField()
    data_venc = models.DateField()
    valor_nut = models.DecimalField(decimal_places=1, max_digits=3)
    custo = models.DecimalField(decimal_places=2, max_digits=8)
    peso = models.DecimalField(decimal_places=2, max_digits=5)

def __str__(self):
    return self.nome