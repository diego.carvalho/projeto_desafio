from django.forms import ModelForm
from .models import Produtos

class ProdutosForm(ModelForm):
    class Meta:
        model = Produtos
        fields = ['nome','cod_prod','data_venc','valor_nut','custo','peso']
