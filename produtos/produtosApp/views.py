from django.shortcuts import render, redirect, get_object_or_404
from .forms import *

#Create
def produtos_new(request, template_name='produto_form.html'):
    form = ProdutosForm(request.POST or None, request.Files or None)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('produtos_list')
    return render(request, template_name, dados)

#Read
def produtos_list(request, template_name ='produtos.html'):
    produtos = Produtos.objects.all()
    dados = {'produtos':produtos}

    return render(request, template_name, dados)

#Update

def produtos_update(request,id,template_name='produtos_form.html'):
    produtos = get_object_or_404(Produtos, pk=id)
    form = ProdutosForm(request.POST or None, request.Files or None, instace = produtos)

    dados = {'form': form}
    if form.is_valid():
        form.save()
        return redirect('produtos_list')
    return render(request, template_name, dados)

#Delete

def produtos_delete(request,id,template_name='produtos_delete.html'):
    produtos = get_object_or_404(Produtos,pk=id)

    if request.method =='POST':
        produtos.delete()
        return redirect('produtos_list')

    return render(request, template_name)
