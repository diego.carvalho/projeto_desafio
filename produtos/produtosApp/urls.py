from django.urls import path
from .views import *

urlpatterns = {
    path('lista/',produtos_list, name ='contatos_list'),
    path('new/',produtos_new,name='contato_new'),
    path ('update/<int:id>/', produtos_update, name = 'contato_update'),
    path ('delete/<int:id>/', produtos_delete, name = 'contato_delete')
}